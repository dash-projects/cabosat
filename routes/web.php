<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('home');
});
Route::get('/satellite-tv','CaboSatController@satelliteTv')->name('satellite-tv');
Route::get('/wifi-solutions','CaboSatController@wifi')->name('wifi-solutions');
Route::get('/home-audio','CaboSatController@audio')->name('home-audio');
Route::get('/about-us','CaboSatController@aboutUs')->name('about-us');
Route::get('/contact-us','CaboSatController@contactUs')->name('contact-us');
Route::get('/contact-form','CaboSatController@contactForm')->name('contact-form-get');
Route::post('/contact-form','CaboSatController@contactForm')->name('contact-form');
Route::get('test-email',function(){
    return view('emails.contact',[
        "nameClient" => "Martin Urias",
  "emailClient" => "urias.martin.soporte@gmail.com",
  "subjectClient" => "Quote Home Audio",
  "contentMessageClient" => "Hi I'm writting this message as test"]);
});
Route::get('/callfromUsCa', function () {
    return Redirect::to('tel:+1-970-778-4572');
});
Route::get('/callfromMex', function () {
    return Redirect::to('tel:+52-1-624-355-3782');
});
