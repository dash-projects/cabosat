<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use ReCaptcha;
use App\Inspections\Spam;

class CaboSatController extends Controller
{
    public function __construct()
    {
    }
    public function satelliteTv(){
        return view('satellite-tv');
    }
    public function wifi(){
        return view('wifi');
    }
    public function audio(){
        return view('home-audio');
    }
    public function aboutUs(){
        return view('about');
    }
    public function contactUs(){
        return view('contact');
    }
    public function contactForm(Request $request): \Illuminate\Http\JsonResponse
    {
        try{
        // Get the form fields and remove whitespace.
            $valid = Validator::make(request()->all(), [
                recaptchaFieldName() => recaptchaRuleName(),
                'name'=>'required',
                'email'=>'required|email',
                'subject'=>'required',
                'message'=>'required'
            ],[
                'recaptcha'=> 'You need to verify that you\'re not a robot'
                ]
            );
            if ($valid->fails())
                return response()->json(['status' => 'error', 'data' => 'Oops! There was an error:<br>*' . join('<br>*', $valid->errors()->all())]);
        $data = $request->all();
        $name = strip_tags(trim($data["name"]));
        $name = str_replace(array("\r","\n"),array(" "," "),$name);
        $email = filter_var(trim($data["email"]), FILTER_SANITIZE_EMAIL);
        $cont_subject = trim($data["subject"]);
        resolve(Spam::class)->detect(trim($data["message"]));
        $message = trim($data["message"]);
        // Check that data was sent to the mailer.
        if ( empty($name) OR empty($cont_subject) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // Set a 400 (bad request) response code and exit.
            return response()->json([ 'status'=>'error','data'=>'Oops! There was a problem with your submission. Please complete the form and try again.']);
        }

        // Set the recipient email address.
        $recipient = [
            "worryfreetv@yahoo.com",
            "cabosat.marcelino@yahoo.com",
            'urias.martin.soporte@gmail.com'
        ];
            $mail = new ContactMail($name,$email,$cont_subject,$message);
        Mail::to($email)->bcc($recipient)->send($mail);
        if (count(Mail::failures()) > 0) {
            return response()->json([ 'status'=>'error','data'=>'Oops! Something went wrong and we couldn\'t send your message.']);
        }
        // Set a 200 (okay) response code.
        return response()->json(['status'=>'success','data'=>'Thank You! Your message has been sent.']);

        }
        catch (\Exception $e){
            return response()->json([ 'status'=>'error','data'=>'Oops! Something went wrong and we couldn\'t send your message. ']);
        }
    }
}
