<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    public $nameClient;
    public $emailClient;
    public $subjectClient;
    public $contentMessageClient;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$email,$subject,$message)
    {
        $this->nameClient= $name;
        $this->emailClient= $email;
        $this->subjectClient= $subject;
        $this->contentMessageClient= $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.contact')->subject('Thanks for contact us - CaboSat.net');
    }
}
