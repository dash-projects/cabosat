<?php

namespace App\Helpers;
use SendGrid\Mail\Mail;
use SendGrid;
class SendGridMail
{
    public $mail;
    private $smtp;
    public $clients;
    /**
     * Create a new SDMail.
     *
     * @param array $from
     * @param string $subject
     * @throws SendGrid\Mail\TypeException
     */
    public function __construct(array $from = ['email' => 'contact@cabosat.net', 'name' => 'Contact CaboSat'], string $subject = "CaboSat")
    {
        $this->mail = new Mail();
        $this->mail->setFrom($from['email'], $from['name']);
        $this->mail->setSubject($subject);
        $this->smtp = new SendGrid(env('SENDGRID_API_KEY'));
    }


    /**
     * @param $subject
     * @return bool
     */
    public function setSubject($subject): bool
    {
        try {
            $this->mail->setSubject($subject);
        } catch (SendGrid\Mail\TypeException $e) {
            return false;
        }
        return true;
    }

    /**
     * @param $client
     * @return bool
     * @throws SendGrid\Mail\TypeException
     */
    public function addClient($client): bool
    {
        $this->mail->addTo($client['email'], isset($client['name']) ? $client['name'] : null);
        return true;
    }

    /**
     * @param $category
     * @return bool
     * @throws SendGrid\Mail\TypeException
     */
    public function addCategory($category): bool
    {
        $this->mail->addCategory($category);
        return true;
    }

    /**
     * @param array $from
     * @return bool
     * @throws SendGrid\Mail\TypeException
     */
    public function setFrom(array $from = ['email' => 'no-reply@nationalcar.com.mx', 'name' => 'National Car México']): bool
    {
        $this->mail->setFrom($from['email'], $from['name']);
        return true;
    }

    /**
     * @param $body_plain
     * @param $body_html
     * @return bool
     * @throws SendGrid\Mail\TypeException
     */
    public function setBodyMail($body_plain, $body_html): bool
    {
        $this->mail->addContent("text/plain", $body_plain);
        $this->mail->addContent("text/html", $body_html);
        return true;
    }

    /**
     * @return int
     */
    public function sendMail(): int
    {
        try {
            $response = $this->smtp->send($this->mail);
            return $response->statusCode();
        } catch (Exception $e) {
            return 'Caught exception: '. $e->getMessage(). "\n";
        }
    }
}