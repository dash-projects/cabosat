@extends('layouts.app')
@section('seo-description','For those looking for English speaking TV in Los Cabos, the options can seem quite daunting and confusing. Since we have over 20 years’ of experience in the Satellite TV business and are constantly keeping up to date on the latest changes, we can give you all the current options.  You can be sure that cabosat.net is your best contact for the ever changing Satellite TV industry.
We can provide you will the US Locals, ABC, CBS, NBC, FOX and PBS, from several locations. Since these locations change from time to time, you will need to contact us for the current options available.
Among the many options that people have used in the past is Shaw Direct from Canada and Dish Network from the United States.
Contact us today for more information.')
@section('seo-keywords','Satellite TV Los Cabos, Los Cabos Satellite TV, Satellite TV Baja Sur')

@section('title', 'Satellite Tv')
@section('menu-satellite','active')
@section('content')
        <h1 class="d-none">Satellite TV</h1>

    @include('section.calltoaction',['title'=>'SHAW DIRECT NO LONGER WORKS IN MEXICO!','id'=>'satellite-tv','content'=>'For those looking for English speaking TV in Los Cabos, Mexico the options can seem
              quite daunting and confusing. Since we have over 20 years of experience in the Satellite TV business and are constantly keeping up to date on the latest changes, we can give you all the current options. You can be sure that
              <a href="https://cabosat.net" target="_blank">cabosat.net</a> is your best contact for the ever changing Satellite TV industry. <br><br>
              We can provide you will the US Locals, ABC, CBS, NBC, FOX and PBS, from several locations. Since these
              locations change from time to time, you will need to contact us for the current options available.<br><br>
              Among the many options that people have used in the past is <i><strong>Shaw Direct</strong></i> from Canada and <i><strong>Dish Network</strong></i> from the United States.<br><br>
              We have a <strong>new alternative to Shaw Direct</strong>. It has USA programming only and works through-out Mexico.<br><br>
              <strong>Contact us today for the latest information on Satellite TV!</strong><br>',
'images'=>[
    ['path'=>asset('/images/satellite/4.png'),'title'=>'Dish Network'],
    ['path'=>asset('/images/satellite/2.png'),'title'=>'ABC Network'],
    ['path'=>asset('/images/satellite/3.png'),'title'=>'FOX'],
    ['path'=>asset('/images/satellite/5.png'),'title'=>'CBS'],
    ['path'=>asset('/images/satellite/1.png'),'title'=>'NBC Network'],
],'action'=>route('contact-us').'#contact-form'])
        {{--@include('section.calltoaction',['title'=>'KiwiSat Satellite TV new to Los Cabos!','id'=>'kiwisat','content'=>'KiwiSat Satellite TV is new to Mexico and the best option for those who need English HD Satellite TV off one small 75 cm dish.<br> 144 High Definition Channels including the US Local Networks (ABC, CBS, NBC, FOX, PBS, CW) The KiwiSat SES-10 Satellite has a very strong signal in all of Mexico including Cabo San Lucas, San Jose del Cabo, Eastcape, Zacatitos, La Fortuna, Cabo Pulmo, La Ribera, Los Barilles, Pescadero, Todos Santos, La Paz and all of Baja California Sur.<br> The Kiwisat signal can be picked up using a dish as small as 18 inches (46 cm) but we recommend a 30 inch (75 cm) dish.<br> We are authorized KiwiSat dealers and can help you get your account set up no matter where you live in Mexico.Check out the KiwiSat channels list below.<br><strong id="kiwi-programming">Kiwisat Channel Listing KP4<br></strong>
  <img src="'.asset('/images/satellite/11.png').'" alt="Kiwisat Channel Listing KP4 Page 1 " style="margin-left: auto;margin-right: auto;margin-top:20px;max-height: 800px;width: 100%;display: block;"><br>
  <img src="'.asset('/images/satellite/12.png').'" alt="Kiwisat Channel Listing KP4 Page 2 " style="margin-left: auto;margin-right: auto;margin-top:20px;max-height: 800px;width: 100%;display: block;"><br>
   The KiwiSat signal is very strong though out Mexico. See the KiwiSat SES-10 Satellite coverage map below.
    <img src="'.asset('/images/satellite/13.png').'" alt="Kiwisat SES-10 Satellite Footprint" style="margin-left: auto;margin-right: auto;margin-top:20px;max-height: 800px;width: 100%;display: block;"><br>Please contact CaboSAT for more information on KiwiSat.'])
--}}@include('section.calltoaction',['title'=>'Dish Network','id'=>'dish-network','content'=>'Dish Network has three Satellites that are currently within the range of Southern Baja. The 119 west,110 West and the 61.5 West.<br>'])
    {{--@include('section.calltoaction',['title'=>'Shaw Direct','id'=>'shaw-direct','content'=>'Shaw Direct broadcasts on two satellites, only the Anik F2 at 111.1 West is within range of Baja Sur<br><br>
    <strong style="font-size:22px">Important update regarding Shaw Direct</strong><br>
    According to <a style="text-decoration: underline;" href="https://www.shawbroadcast.com/Content/US/HEVC.htm" target="_blank">shawbroadcast</a>:
  <i>“ANIK F2 will reach end of life December 2025, at which time Shaw Broadcast will only have signals available on one satellite, ANIK G1.”</i><br><br>'])--}}
    @include('section.calltoaction',['title'=>'Satellite Foot Prints and Technical Data','content'=>'<strong id="echostar-xiv">
  Satellite Name: Echostar 14 (Echostar XIV)<br>
  Position: 119° W (119° W)<br>
  Launch date: 21-Mar-2010<br>
  Expected lifetime: 15 yrs.<br></strong><br>
  <img src="'.asset('/images/satellite/6.png').'" alt="Echostar 14 (Echostar XIV) Foot Prints" style="margin-left: auto;margin-right: auto;margin-top:20px;max-height: 800px;width: 100%;display: block;"><br><br>
  <strong id="echostar-xxiii">
  Satellite Name: Echostar 23 (Echostar XXIII)<br>
  Position: 110° W (109.9° W)<br>
  Launch date: 16-Mar-2017<br>
  Expected lifetime: 15 yrs<br></strong><br>
  <img src="'.asset('/images/satellite/14.png').'" alt="Echostar 23 (Echostar XXIII)" style="margin-left: auto;margin-right: auto;margin-top:20px;max-height: 800px;width: 100%;display: block;"><br><br>
  <strong id="echostar-xv">
  Satellite Name: Echostar 15 (Echostar XV)<br>
  Position: 61° W (61.55° W)<br>
  Launch date: 10-Jul-2010<br>
  Expected lifetime: 15 yrs<br></strong><br>
  <img src="'.asset('/images/satellite/15.png').'" alt="Echostar 15 (Echostar XV)" style="margin-left: auto;margin-right: auto;margin-top:20px;max-height: 800px;width: 100%;display: block;"><br><br>',
    ])
        @include('section.calltoaction',['title'=>'Sun outage or Sun Transit effects on Satellite TV','id'=>'sun-outage','content'=>'From <a style="text-decoration: underline;" href="https://en.wikipedia.org/wiki/Sun_outage" target="_blank" >Wikipedia</a>, the free encyclopedia<br><br>
    <i>"A Sun outage, Sun transit, or Sun fade is an interruption in or distortion of geostationary satellite signals
    caused by interference (background noise) of the Sun when it falls directly behind a satellite which an Earth
    station is trying to receive data from or transmit data to. It usually occurs briefly to such satellites twice
    per year and such earth stations install temporary or permanent guards to their receiving systems to prevent
    equipment damage." [...]</i><br><br>
    <i>"Sun outages occur before the March equinox (in February and March) and after the September equinox
    (in September and October) for the Northern Hemisphere, and occur after the March equinox and before the September
    equinox for the Southern Hemisphere. At these times, the apparent path of the Sun across the sky takes it directly
    behind the line of sight between an earth station and a satellite. The Sun radiates strongly across the entire
    spectrum, including the microwave frequencies used to communicate with satellites (C band, Ku band, and Ka band),
    so the Sun swamps the signal from the satellite. The effects of a Sun outage range from partial degradation
    (increase in the error rate) to the total destruction of the signal. The effect sweeps from north to south from
    approximately 20 February to 20 April, and from south to north from approximately 20 August to 20 October,
    affecting any specific location for less than 12 minutes a day for a few consecutive days."</i><br><br>
    <h3> Useful links: </h3>
    <a style="text-decoration: underline;" href="https://www.satellite-calculations.com/Satellite/suninterference.php" target="_blank">Satellite Calculations - Sun Outage / Sun Interference Prediction for Geostationary Orbit Satellites</a><br>
    <a style="text-decoration: underline;" href="https://www.satellite-calculations.com/Satellite/sunoutagestatus.php" target="_blank">Satellite Calculations - Sun Outage Visualizer Simulator for Geostationary Satellites</a><br>

    <br><br>'])
@endsection
