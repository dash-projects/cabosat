<!-- Call to Action -->
<div id="mu-call-to-action">
  <div class="container">
    <div class="row"  @if(isset($id))id="{{$id}}"@endif>
      <div class="col-md-12">
        <div class="mu-call-to-action-area">
          <div class="mu-call-to-action-left">
            <h2>{{$title}}</h2>
            <br/>
            <p>{!! $content !!}</p>
          </div>
          @if(isset($action))<a href="{{$action}}" class="mu-primary-btn mu-quote-btn">Get a Quote <i class="fa fa-angle-right"></i></a>@endif
          @if(isset($images) && count($images)>0)
            <div class="row col-md-12">
              @foreach($images as $img)
                <div class="col-md-3" style="text-align: center;">
                  <img src="{{$img['path']}}" alt="{{$img['title']}}" style="margin-left: auto;margin-right: auto;margin-top:30px;max-height: 135px;display: block;">
                </div>
              @endforeach
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
