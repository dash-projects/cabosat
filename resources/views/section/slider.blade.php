<!-- Start slider area -->
<div id="mu-slider">
  <div class="mu-slide">
    <!-- Start single slide  -->
    <div class="mu-single-slide">
      <img src="{{asset('/images/bg-home.jpg')}}" alt="Welcome To CaboSAT" title="Welcome To CaboSAT" style="width: 100%">
      <div class="mu-single-slide-content-area">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="mu-single-slide-content">
                <h1>Welcome to CaboSat</h1>
                <p class="d-md-none">We offer cutting edge Satellite TV, Wifi Networking and Audio solutions for your home or business. Our services are designed for anyone living in Baja Sur, Mexico.</p>
                <h2 class="d-none d-md-block">We offer cutting edge Satellite TV, Wifi Networking and Audio solutions for your home or business. Our services are designed for anyone living in Baja Sur, Mexico.</h2>
                <a class="mu-primary-btn" href="#mu-about">Read more <span class="fas fa-angle-right"></span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End single slide  -->

    <!-- Start single slide  -->
        <div class="mu-single-slide">
          <img src="{{asset('/images/slider-img-1.jpg')}}" alt="slider img" style="width: 100%">
          <div class="mu-single-slide-content-area">
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <div class="mu-single-slide-content">
                    <h1>Get all your Favorite Channels anywhere </h1>
                    <p class="d-md-none">For all your Satellite TV needs, Contact us today!</p>
                    <h2 class="d-none d-md-block">For all your Satellite TV needs, Contact us today!</h2>
                    <a class="mu-primary-btn" href="{{route('satellite-tv')}}">Read more <span class="fas fa-angle-right"></span></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    <!-- End single slide  -->

    <!-- Start single slide  -->
    <div class="mu-single-slide">
      <img src="{{asset('/images/slider-img-5.jpg')}}" alt="slider img" style="width: 100%">
      <div class="mu-single-slide-content-area">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="mu-single-slide-content">
                <h1>Do you need WIFI?</h1>
                <p class="d-md-none">Get it today! CaboSAT has a WIFI solutions for you.</p>
                <h2 class="d-none d-md-block">Get it today! CaboSAT has a WIFI solutions for you.</h2>
                <a class="mu-primary-btn" href="{{route('wifi-solutions')}}">Read more <span class="fas fa-angle-right"></span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End single slide  -->
    <!-- Start single slide  -->
    <div class="mu-single-slide">
      <img src="{{asset('/images/slider-img-6.jpg')}}" alt="slider img" style="width: 100%">
      <div class="mu-single-slide-content-area">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="mu-single-slide-content">
                <h1>Get The Best Home Audio</h1>
                <p class="d-md-none">Check our home audio solutions specially made for you.</p>
                <h2 class="d-none d-md-block">Check our home audio solutions specially made for you.</h2>
                <a class="mu-primary-btn" href="{{route('home-audio')}}">Read more <span class="fas fa-angle-right"></span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End single slide  -->

  </div>
</div>
<!-- End Slider area -->
