<!-- Start About -->
<section id="mu-about">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="mu-about-area">
          <!-- Title -->
          <div class="row">
            <div class="col-md-12">
              <div class="mu-title">
                <h2>Who we are</h2>
                <p class="text-justify">Founded by Mark Binkley in 2003, our company is based on the core values of integrity, reliability and
                  performance. Fresh out of College in the 1980’s, Mark Binkley, spent the better part of 10 years learning
                  the business from one of the industry’s leading companies.
                  “I was privileged to learn from some of the industry’s best. They taught me that quality installation was
                  only the beginning and that ongoing customer service was the key to a successful business. I have
                  maintained this attitude ever since.”
                  Before settling in Los Cabos, Mexico, Mark Binkley, owned and operated successful Low Voltage Audio
                  Video businesses in Western Colorado and Seattle, Washington. We also provide Sales and service in
                  Keizer, Oregon.
                  We make it our goal to keep up with the ever changing technology so that we can offer you a cutting-
                  edge system that will fulfill your vision of a worry free, state of the art home.
                  Our courteous well trained staff is ready to take care of all your needs.
                  <br/><br/>
                  <i>“I was privileged to learn from some of the industry's best. They taught me that quality installation was only the beginning and that ongoing customer service was the key to a successful business. I have maintained this attitude ever since.”</i> <b>- Mark A Binkley</b></p>
              </div>
            </div>
          </div>
          <!-- Start Feature Content -->
          <div class="row">
            <div class="col-md-6">
              <div class="mu-about-left">
                <img src="{{asset('img/bg-1.jpg')}}" alt="Satellite TV">
              </div>
            </div>
            <div class="col-md-6">
              <div class="mu-about-right">
                <ul>
                  <li>
                    <h3>Our Mission</h3>
                    <p>To Provide personalized custom home solutions tailored to fit your individual needs</p>
                  </li>
                  <li>
                    <h3>Our Vision</h3>
                    <p>To be the company that provides the highest quality products and service possible.</p>
                  </li>
                  <li>
                    <h3>Our Values</h3>
                    <p>Responsibility, Passion, Professionalism, Honesty, Commitment, Trust, Family.</p>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <!-- End Feature Content -->
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End About -->
