<!-- Start Header -->
<header id="mu-hero">
<div class="container">
  <nav class="navbar navbar-expand-lg navbar-light mu-navbar">
    <!-- Text based logo -->
    <!-- <a class="navbar-brand mu-logo" href="index.html"><span>B-HERO</span></a> -->
    <!-- image based logo -->
      <a class="navbar-brand mu-logo" href="/"><img src="{{asset('/img/logo-cabosat.png')}}" alt="CaboSat"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="fa fa-bars"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto mu-navbar-nav">
        <li class="nav-item @yield('menu-home')"><a href="/">Home</a></li>
        <li class="nav-item  @yield('menu-satellite')"><a href="{{route('satellite-tv')}}">Satellite TV</a></li>
        <li class="nav-item  @yield('menu-wifi')"><a href="{{route('wifi-solutions')}}">Wifi Solutions</a></li>
        <li class="nav-item  @yield('menu-audio')"><a href="{{route('home-audio')}}">Home Audio</a></li>
        <li class="nav-item @yield('menu-about')"><a href="/about-us">About us</a></li>
        <li class="nav-item @yield('menu-contact')"><a href="/contact-us">Contact us</a></li>
          <!-- <li class="nav-item"><a href="404.html">404 Page</a></li> -->
      </ul>
    </div>
  </nav>
</div>
</header>
<!-- End Header -->
