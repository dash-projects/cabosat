<!-- Start Services -->
<section id="mu-service">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="mu-service-area">
          <!-- Title -->
          <div class="row">
            <div class="col-md-12">
              <div class="mu-title">
                <h2>Our exclusive services</h2>
                <p>Take a look at all the services that cabosat can offer you</p>
              </div>
            </div>
          </div>
          <!-- Start Service Content -->
          <div class="row">
            <div class="col-md-12">
              <div class="mu-service-content">
                <div class="row">
                  <!-- Start single service -->
                  <div class="col-md-4">
                    <div class="mu-single-service">
                      <div class="mu-single-service-icon"><i class="fas fa-tv" aria-hidden="true"></i></div>
                      <div class="mu-single-service-content">
                        <h3>Satellite TV</h3>
                        <p>Your best contact for the ever changing Satellite TV industry. We can provide you will the US Locals, ABC, CBS, NBC, FOX and PBS, from several locations.</p>
                      </div>
                    </div>
                  </div>
                  <!-- End single service -->
                  <!-- Start single service -->
                  <div class="col-md-4">
                    <div class="mu-single-service">
                      <div class="mu-single-service-icon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                      <div class="mu-single-service-content">
                        <h3>Security Alarm Systems</h3>
                        <p>Keep your home or business secure and monitored 24/7</p>
                      </div>
                    </div>
                  </div>
                  <!-- End single service -->
                  <!-- Start single service -->
                  <div class="col-md-4">
                    <div class="mu-single-service">
                      <div class="mu-single-service-icon"><i class="fa fa-sitemap" aria-hidden="true"></i></div>
                      <div class="mu-single-service-content">
                        <h3>Networking</h3>
                        <p>Wifi and Networking Solutions for your home or business</p>
                      </div>
                    </div>
                  </div>
                  <!-- End single service -->
                  <!-- Start single service -->
                  <div class="col-md-4">
                    <div class="mu-single-service">
                      <div class="mu-single-service-icon"><i class="fa fa-volume-up" aria-hidden="true"></i></div>
                      <div class="mu-single-service-content">
                        <h3>Home Audio</h3>
                        <p>Everyone Loves Music! When you are relaxing outside by the pool or inside enjoying a meal, music is important to you. We have speakers for every room, both inside and out</p>
                      </div>
                    </div>
                  </div>
                  <!-- End single service -->
                  <!-- Start single service -->
                  <div class="col-md-4">
                    <div class="mu-single-service">
                      <div class="mu-single-service-icon"><i class="fa fa-clock" aria-hidden="true"></i></div>
                      <div class="mu-single-service-content">
                        <h3>Customer Support</h3>
                        <p>Don't worry about your tv signal or internet, we will solve the problem</p>
                      </div>
                    </div>
                  </div>
                  <!-- End single service -->
                  <!-- Start single service -->
                  <div class="col-md-4">
                    <div class="mu-single-service">
                      <div class="mu-single-service-icon"><i class="fa fa-cog" aria-hidden="true"></i></div>
                      <div class="mu-single-service-content">
                        <h3>Customization TV Programing or Networking</h3>
                        <p>There is a service especially for you, tell us your needs and we will make it happen</p>
                      </div>
                    </div>
                  </div>
                  <!-- End single service -->
                </div>
              </div>
            </div>
          </div>
          <!-- End Service Content -->
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End Services -->
