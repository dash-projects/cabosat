@extends('layouts.app')
@section('seo-description','Welcome to CaboSat, We offer cutting edge Satellite TV, Wifi Networking and Audio solutions for your home or business. Our services are designed for anyone living in Baja Sur, Mexico.
Get all your Favorite Channels anywhere , For all your Satellite TV needs, Contact us today!. Do you need WIFI? Get it today! CaboSAT has a WIFI solutions for you. Get The Best Home Audio, Check our home audio solutions specially made for you. Get a quote today')
@section('seo-keywords','Satellite TV Los Cabos, Los Cabos Satellite TV, Satellite TV Baja Sur')
@section('title', 'Home Page')
@section('menu-home','active')
@section('preContent')
    @include('section.slider')
@endsection
@section('content')
    @include('section.services')
    @include('section.calltoaction',['title'=>'Do you still have doubts?','content'=>'Don\'t worry, we will help you. Get a quote now!<br>','action'=>route('contact-us').'#contact-form'])
    @include('section.about')

@endsection
