@extends('layouts.base')
@section('title','CaboSat Website under construction')
@section('header')
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('/comingsoon/vendor/bootstrap/css/bootstrap.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('/comingsoon/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('/comingsoon/vendor/animate/animate.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('/comingsoon/vendor/select2/select2.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('/comingsoon/css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/comingsoon/css/main.css')}}">
    <!--===============================================================================================-->
@endsection
@section('content')
    <div class="size1 bg0 where1-parent">
        <!-- Coutdown -->
        <div class="flex-c-m bg-img1 size2 where1 overlay1 where2 respon2" style="background-image: url('{{asset('img/bg-1.jpg')}}');">
            <div class="wsize2 flex-w flex-c-m cd100 js-tilt">
                <div class="flex-col-c-m size6 bor2 m-l-10 m-r-10 m-t-15">
                    <span class="l2-txt1 p-b-9 days">35</span>
                    <span class="s2-txt4">Days</span>
                </div>

                <div class="flex-col-c-m size6 bor2 m-l-10 m-r-10 m-t-15">
                    <span class="l2-txt1 p-b-9 hours">17</span>
                    <span class="s2-txt4">Hours</span>
                </div>

                <div class="flex-col-c-m size6 bor2 m-l-10 m-r-10 m-t-15">
                    <span class="l2-txt1 p-b-9 minutes">50</span>
                    <span class="s2-txt4">Minutes</span>
                </div>

                <div class="flex-col-c-m size6 bor2 m-l-10 m-r-10 m-t-15">
                    <span class="l2-txt1 p-b-9 seconds">39</span>
                    <span class="s2-txt4">Seconds</span>
                </div>
            </div>
        </div>

        <!-- Form -->
        <div class="size3 flex-col-sb flex-w p-l-75 p-r-75 p-t-45 p-b-45 respon1">
            <div class="wrap-pic1">
                <img src="{{asset('/img/logo-cabosat.png')}}" alt="Baja Green" title="Baja Green">
            </div>

            <div class="p-t-50 p-b-60">
                <p class="m1-txt1 p-b-36">
                    Our website is <span class="m1-txt2">Coming Soon</span>, contact us for a site survey now!
                </p>

                <form class="contact100-form validate-form">
                    <div class="w-full">
                        <a href="/callfromUsCa" class="flex-c-m s2-txt2 size4 bg1 bor1 hov1 trans-04">US/Can 970-778-4572 / Call Us &nbsp;&nbsp;<i class="fa fa-phone"></i></a>
                        <br>
                        <a href="/callfromMex" class="flex-c-m s2-txt2 size4 bg1 bor1 hov1 trans-04">Mex 624-355-3782 / Call Us &nbsp;&nbsp;<i class="fa fa-phone"></i></a>
                    </div>
                </form>

                <p class="s2-txt3 p-t-18">
                    And don’t worry, we're willing to help you.
                </p>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

    <!--===============================================================================================-->
    <script src="{{asset('/comingsoon/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('/comingsoon/vendor/countdowntime/moment.min.js')}}"></script>
    <script src="{{asset('/comingsoon/vendor/countdowntime/moment-timezone.min.js')}}"></script>
    <script src="{{asset('/comingsoon/vendor/countdowntime/moment-timezone-with-data.min.js')}}"></script>
    <script src="{{asset('/comingsoon/vendor/countdowntime/countdowntime.js')}}"></script>
    <script>
        $('.cd100').countdown100({
            /*Set Endtime here*/
            /*Endtime must be > current time*/
            endtimeYear: 2022,
            endtimeMonth: 3,
            endtimeDate: 31,
            endtimeHours: 6,
            endtimeMinutes: 0,
            endtimeSeconds: 0,
            timeZone: "America/Mazatlan"
            // ex:  timeZone: "America/New_York"
            //go to " http://momentjs.com/timezone/ " to get timezone
        });
    </script>
    <!--===============================================================================================-->
    <script src="{{asset('/comingsoon/vendor/tilt/tilt.jquery.min.js')}}"></script>
    <script >
        $('.js-tilt').tilt({
            scale: 1.1
        })
    </script>
    <!--===============================================================================================-->
    <script src="{{asset('/comingsoon/js/main.js')}}"></script>
@endsection
