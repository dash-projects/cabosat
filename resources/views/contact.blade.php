@extends('layouts.app')
@section('title', 'Contact Page')
@section('menu-contact','active')
@section('content')
    @include('section.contact')
@endsection
