@extends('layouts.app')

@section('title', 'About us')
@section('menu-about','active')
@section('content')
    @include('section.about')
@endsection
