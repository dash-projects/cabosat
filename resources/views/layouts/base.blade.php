<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="author" content="cabodash.com">
      <!-- Meta Description -->
      <meta name="description" content="We offer cutting edge Satellite TV, WI-Fi Networking and Audio solutions for your home or business. Our
services are designed for anyone living in Baja Sur, Mexico.">
      <!-- Meta Keyword -->
      <meta name="keywords"
            content="CaboSat, wifi los cabos, satellite tv in los cabos, worryfree tv los cabos, worryfree av, satellite tv baja sur mexico">
      <!-- meta character set -->
      <meta charset="UTF-8">

      {{--    Secciones de SEO--}}
      <meta property=”og:type” content=”article”/>
      <meta property=”og:title” content=”@yield('title')”/>
      <meta property=”og:description” content=”We offer cutting edge Satellite TV, WI-Fi Networking and Audio solutions for your home or business. Our services are designed for anyone living in Baja Sur, Mexico.”/>
      <meta property=”og:image” content=”https://cabosat.net/img/logo-cabosat.png”/>
      <meta property=”og:url” content=”https://cabosat.net”/>
      <meta property=”og:site_name” content=”CaboSat”/>
      <meta name=”twitter:title” content=”@yield('title')”>
      <meta name=”twitter:description” content=”We offer cutting edge Satellite TV, WI-Fi Networking and Audio solutions for your home or business. Our services are designed for anyone living in Baja Sur, Mexico.”>
      <meta name=”twitter:image” content=”https://cabosat.net/img/logo-cabosat.png”>
{{--      <meta name=”twitter:site” content=”>--}}
{{--      <meta name=”twitter:creator” content=”@BajaGreenMx”>--}}
    <title>@yield('title') - CaboSat</title>
    <!-- Favicon -->
    <!-- <link rel="shortcut icon" type="image/icon" href="/images/favicon.ico"/> -->
    <link rel="apple-touch-icon" sizes="57x57" href="/images/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/images/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/icons/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
      @yield('header')
    <!-- Font Awesome -->
    <link href="/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet">
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/bootstrap/dist/css/bootstrap.min.css" >
    <!-- Slick slider -->
    <link href="/css/slick.css" rel="stylesheet">
    <!-- Gallery Lightbox -->
    <link href="/css/magnific-popup.css" rel="stylesheet">
    <!-- Skills Circle CSS  -->
    <link rel="stylesheet" type="text/css" href="/css/circle.css">


    <!-- Main Style -->
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/common.css" rel="stylesheet">

    <!-- Fonts -->

    <!-- Google Fonts Raleway -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,400i,500,500i,600,700" rel="stylesheet">
	<!-- Google Fonts Open sans -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,700,800" rel="stylesheet">
    {!! htmlScriptTagJsApi() !!}
    <!-- IMPORTANT!!! remember CSRF token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script type="text/javascript">
      function callbackThen(response){
        // read HTTP status
        console.log(response.status);

        // read Promise object
        response.json().then(function(data){
          console.log(data);
        });
      }
      function callbackCatch(error){
        console.error('Error:', error)
      }
    </script>
{{--    {!! htmlScriptTagJsApiV3([--}}
{{--        'action' => 'homepage',--}}
{{--        /*'callback_then' => 'callbackThen',--}}
{{--        'callback_catch' => 'callbackCatch'*/--}}
{{--    ]) !!}--}}
    @yield('style')

	</head>
  <body>

   <!--START SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#">
      <i class="fa fa-angle-up"></i>
    </a>
  <!-- END SCROLL TOP BUTTON -->
	<!-- Start main content -->
	<main>
      @yield('content')
	</main>

	@yield('scripts')

  </body>
</html>
