<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title') - CaboSat</title>
    <meta name="description" content="@yield('seo-description')">
    <meta name="keywords" content="@yield('seo-keywords')">
    <meta name="author" content="CaboSAT">
    <!-- Favicon -->
    <!-- <link rel="shortcut icon" type="image/icon" href="/images/favicon.ico"/> -->
    <link rel="apple-touch-icon" sizes="57x57" href="/images/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/images/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/icons/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
      <meta name="theme-color" content="#ffffff">
{{--      TODO:SEO--}}
      <!--SEO -->
      <meta property="og:title" content="@yield('title') - CaboSat"/>
      <meta property="og:type" content="website"/>
      <meta property="og:site_name" content="CaboSAT.net"/>
      <meta property="og:description" content="@yield('seo-description')">
      <meta property="og:image" content="@yield('seo-image')"/>
      <meta property="og:url" content="{{url()->full()}}"/>
      <meta name="twitter:title" content="@yield('title') - CaboSat">
      <meta name="twitter:description" content="@yield('seo-description')">
      <meta name="twitter:image" content="@yield('seo-image')">
{{--      The card type, which will be one of “summary”, “summary_large_image”, “app”, or “player”.--}}
      <meta name="twitter:card" content="summary">

      <!-- Font Awesome -->
    <link href="/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet">
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/bootstrap/dist/css/bootstrap.min.css" >
    <link rel="stylesheet" href="{{asset('/comingsoon/fonts/font-awesome-4.7.0/css/font-awesome.css')}}" >
    <!-- Slick slider -->
    <link href="/css/slick.css" rel="stylesheet">
    <!-- Gallery Lightbox -->
    <link href="/css/magnific-popup.css" rel="stylesheet">
    <!-- Skills Circle CSS  -->
    <link rel="stylesheet" type="text/css" href="/css/circle.css">


    <!-- Main Style -->
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/common.css" rel="stylesheet">

    <!-- Fonts -->

    <!-- Google Fonts Raleway -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,400i,500,500i,600,700" rel="stylesheet">
	<!-- Google Fonts Open sans -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,700,800" rel="stylesheet">
    {!! htmlScriptTagJsApi() !!}
  @yield('style')
      <style>
          .whatsapp-floating{
              right: 20px;
              top: 60px;
              width: 55px;
              height: 55px;
              z-index: 1000;
              text-align: center;
              border-radius: 50%;
              position: fixed;
          }
          .whatsapp-floating .fa {
              font-size: 2rem;
              padding-top: 4px;
              padding-bottom: 4px;
          }
      </style>

	</head>
  <body>

   <!--START SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#">
      <i class="fa fa-angle-up"></i>
    </a>
  <!-- END SCROLL TOP BUTTON -->
@include('section.top-phone')
@include('section.header-menu')

   @yield('preContent')
	<!-- Start main content -->
	<main>
      @yield('content')
	</main>
   <div class="fixed-top">
     <div class="float-right">
       <a href="https://wa.me/+526243553782?text=Hello%20I%20come%20from%20cabosat.net%20I%20want%20a%20quote" target="_blank" class="btn btn-outline-success whatsapp-floating"><i class="fa fa-whatsapp"></i></a>
     </div>
   </div>
	<!-- End main content -->
@include('section.footer')




	<!-- JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/popper.min.js"></script>
    <script type="text/javascript" src="/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- Slick slider -->
    <script type="text/javascript" src="/js/slick.min.js"></script>
    <!-- Progress Bar -->
    <script type="text/javascript" src="/js/circle.js"></script>
    <!-- Filterable Gallery js -->
    <!-- <script type="text/javascript" src="/js/jquery.filterizr.min.js"></script> -->
    <!-- Gallery Lightbox -->
    <script type="text/javascript" src="/js/jquery.magnific-popup.min.js"></script>
    <!-- Counter js -->
    <!-- <script type="text/javascript" src="/js/counter.js"></script> -->
    <!-- Ajax contact form  -->
    <script type="text/javascript" src="/js/app.js"></script>


    <!-- Custom js -->
	<script type="text/javascript" src="/js/custom.js"></script>

	@yield('scripts')

  </body>
</html>
