@extends('layouts.app')
@section('title', 'Whole Home Audio')
@section('menu-audio','active')
@section('content')
    @include('section.calltoaction',['title'=>'Whole Home Audio','content'=>'Everyone Loves Music! When you are relaxing
 outside by the pool or inside enjoying a meal, music is important to you. We have speakers for every room, both inside
 and out. Our speakers can be flush mounted in the walls or ceilings, so you don’t notice them. Of course whatever your
 budget, we have great sounding speakers for you.<br><br>',
 'images'=>[
     ['path'=>asset('/images/audio/1.png'),'title'=>'Home Audio speaker'],
     ['path'=>asset('/images/audio/2.png'),'title'=>'Home Audio stay room'],
     ['path'=>asset('/images/audio/3.png'),'title'=>'Home Audio speaker design'],
     ['path'=>asset('/images/audio/4.png'),'title'=>'KEF brand Innovator in sound'],
     ['path'=>asset('/images/audio/5.png'),'title'=>'Polkaudio brand'],
     ['path'=>asset('/images/audio/6.png'),'title'=>'Origin Acoustics brand'],
     ['path'=>asset('/images/audio/7.png'),'title'=>'SONOS brand'],
],'action'=>route('contact-us').'#contact-form'])
    @include('section.calltoaction',['title'=>'Distributed Whole Home Audio','content'=>'Listen to your favorite Music
anywhere in your house. Today you have a lot of options to play music throughout your home. You can either use a single
source of multi-source system. The single source system plays the same music throughout all the rooms.<br>'
 ,'action'=>route('contact-us').'#contact-form'])
    @include('section.calltoaction',['title'=>'Multi Zone Audio System','content'=>'With our Multi Zoned systems you can
 listen to different music in different rooms using different sources. All can be controlled with a remote or keypad.
 Worryfree AV offers simple multi-zone audio systems with full control of many different sources such as of CD players,
 iPods and satellite or IP radio.<br><br>',
 'images'=>[
     ['path'=>asset('/images/audio/8.png'),'title'=>'Audio System schema 1'],
     ['path'=>asset('/images/audio/9.png'),'title'=>'Audio System schema 2'],
     ['path'=>asset('/images/audio/10.png'),'title'=>'Russound brand'],
     ['path'=>asset('/images/audio/11.png'),'title'=>'YAMAHA brand'],
     ['path'=>asset('/images/audio/12.png'),'title'=>'Russound Certified Installer'],
]
,'action'=>route('contact-us').'#contact-form'])
    @include('section.calltoaction',['title'=>'Wireless Music Anywhere','content'=>'No wires no worries! Enjoy crystal
clear HiFI sound anywhere in your house, no wires needed! We offer many different options including industry leading
brands such as SONOS and YAMAHA.<br><br>',
 'images'=>[
     ['path'=>asset('/images/audio/13.png'),'title'=>'SONOS Speaker'],
     ['path'=>asset('/images/audio/14.png'),'title'=>'SONOS Controller'],
     ['path'=>asset('/images/audio/15.png'),'title'=>'MusicCast your home of sound'],
]
,'action'=>route('contact-us').'#contact-form'])
@endsection
