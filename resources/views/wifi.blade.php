@extends('layouts.app')
@section('title', 'Wifi Solutions')
@section('menu-wifi','active')
@section('content')
    @include('section.calltoaction',['title'=>'Wifi Solutions','content'=>"CaboSAT “Worry Free WI-FI Systems” WiFi is the backbone of the modern home and CaboSAT is the very best option to keep your home connected.<br>Contact us to discuss your personal WIFI needs<br><br>
<h3>Ubiquiti Networks</h3>
<img src='/images/PartnerPage-Logos-Ubiquiti.png' alt='Ubiquiti'>
<h3>Araknis Networking</h3>
<img src='/images/Arankis.jpg' alt='Araknis Networking'>",'action'=>'/quote'])
@endsection
